import React, { useState } from 'react';
import './QuantitySelector.css';

const QuantitySelector = ({ initialQuantity = 1, onChange }) => {
  const [quantity, setQuantity] = useState(initialQuantity);

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
      onChange(quantity - 1);
    }
  };

  const handleIncrement = () => {
    setQuantity(quantity + 1);
    onChange(quantity + 1);
  };

  return (
    <div className="quantity-selector">
      <button className="quantity-btn" onClick={handleDecrement}>-</button>
      <span className="quantity-display">{quantity}</span>
      <button className="quantity-btn" onClick={handleIncrement}>+</button>
    </div>
  );
};

export default QuantitySelector;
