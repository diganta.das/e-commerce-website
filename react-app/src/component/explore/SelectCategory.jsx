import React, { useState } from "react";
import "./SelectCategory.css";

function SelectCategory({ checkBoxState, handleCheckBox }) {
  const [isOpen, setIsOpen] = useState(false);

  function handleCategoryClick() {
    setIsOpen(!isOpen);
  }

  return (
    <div className="select-category_container">
      <h4 onClick={handleCategoryClick}>Category</h4>
      <div className={isOpen ? "open" : ""}>
        <span className="category-option">
          <input
            type="checkbox"
            id="category-men"
            name="game"
            checked={checkBoxState.game}
            onChange={handleCheckBox}
          />
          <label htmlFor="category-men">Games</label>
        </span>
        <span className="category-option">
          <input
            type="checkbox"
            id="category-women"
            name="game_acc"
            checked={checkBoxState.game_acc}
            onChange={handleCheckBox}
          />
          <label htmlFor="category-women">Gaming Accessories</label>
        </span>
      </div>
    </div>
  );
}

export default SelectCategory;
