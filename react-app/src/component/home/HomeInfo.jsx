import React from "react";
import "./HomeInfo.css";
import { Link } from "react-router-dom";

function HomeInfo() {
  return (
    <article className="home-info">
      <div className="info-txt">
        <h2>Experience the Ultimate in Gaming with Our Premium Gear</h2>
        <p>
          Dive into a world where cutting-edge technology meets immersive
          gameplay and unbeatable performance. Our handpicked selection includes
          the latest consoles, state-of-the-art accessories, and must-have games
          designed to elevate your gaming experience to new heights. Whether
          you're battling foes, exploring new worlds, or racing to the finish
          line, our gear ensures you stay ahead of the competition. Join the
          ranks of elite gamers who demand nothing but the best in precision,
          speed, and excitement.
        </p>
      </div>
      <button className="explore-clothing_btn">
        <Link to="explore/all">Discover Our Products</Link>
      </button>
    </article>
  );
}

export default HomeInfo;
