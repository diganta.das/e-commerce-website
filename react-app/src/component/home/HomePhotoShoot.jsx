import React from "react";

import "./HomePhotoShoot.css";

function HomePhotoShoot() {
  return (
    <div className="photoshoot-container">
      <span className="model-photo_wrapper boy">
        <img
          src="https://upload.wikimedia.org/wikipedia/en/a/a3/Official_cover_art_for_Bioshock_Infinite.jpg"
          className="model-photo"
          alt="model photograph"
        />
      </span>
      <span className="model-photo_wrapper boy">
        <img
          src="https://th.bing.com/th/id/OIP.K636DsnaUZRP8R3Z4orARgHaHa?rs=1&pid=ImgDetMain"
          className="model-photo"
          alt="model photograph"
        />
      </span>
      <span className="model-photo_wrapper female">
        <img
          src="https://image.api.playstation.com/cdn/EP0002/CUSA05379_00/iTxbX14rj7Qhk3zYc6bnmDiuXMIK2UUW.png"
          className="model-photo"
          alt="model photograph"
        />
      </span>
    </div>
  );
}

export default HomePhotoShoot;
