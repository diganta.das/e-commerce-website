let baseUrl = "https://fakestoreapi.com";

const GAMES_DATA = [
  {
    id: 1,
    title: "Elder Scrolls V: Skyrim",
    description:
      "Elder Scrolls V: Skyrim is an open-world action RPG developed by Bethesda Game Studios. Players can explore a vast, detailed world filled with mountains, forests, dungeons, and towns. The game allows for a high degree of character customization, including choosing race, skills, and abilities. Players engage in combat with a variety of weapons and magic, complete quests given by NPCs, and develop their character's skills over time. The main story revolves around the Dragonborn, a hero prophesied to save the world from the return of dragons, but there are numerous side quests and factions to join, providing hours of gameplay.",
    image:
      "https://m.media-amazon.com/images/I/615XrdCOJsS._AC_SX679_.jpg",
    price: "3999",
    category: "game",
  },
  {
    id: 2,
    title: "Battlefield 1",
    description:
      "Battlefield 1 is a first-person shooter developed by DICE and published by Electronic Arts, set during World War I. The game features large-scale multiplayer battles with up to 64 players, utilizing a variety of vehicles such as tanks, planes, and horses, along with an extensive arsenal of period-appropriate weapons. The single-player campaign consists of several war stories, each focusing on different soldiers and perspectives from various fronts of the war. The game is praised for its immersive atmosphere, realistic graphics, and dynamic weather effects that impact gameplay.",
    image:
      "https://image.api.playstation.com/cdn/EP0006/CUSA02387_00/bgWtVB6cCIhNpWPdAW3UDdvpve8kxic0.png",
    price: "2999",
    category: "game",
  },
  {
    id: 3,
    title: "Call of Duty: WWII",
    description:
      'Call of Duty: WWII is a first-person shooter developed by Sledgehammer Games and published by Activision. It returns to the series\' roots with a setting in World War II. The game features a gripping single-player campaign following the story of Ronald "Red" Daniels, a soldier in the 1st Infantry Division, as they fight across Europe. The multiplayer mode includes classic game types and introduces the War mode, where players engage in objective-based battles. The game also features a cooperative Zombies mode with a unique storyline and challenging gameplay.',
    image:
      "https://th.bing.com/th/id/OIP.K636DsnaUZRP8R3Z4orARgHaHa?rs=1&pid=ImgDetMain",
    price: "4999",
    category: "game",
  },
  {
    id: 4,
    title: "Crysis Remastered",
    description:
      "Crysis Remastered is a visually enhanced version of the classic sci-fi first-person shooter developed by Crytek. Players take on the role of Nomad, a super-soldier equipped with a powerful nanosuit that provides enhanced strength, speed, armor, and cloaking abilities. Set in the year 2020, the game takes place on a fictional island in the Philippines, where players must combat both North Korean soldiers and alien invaders. The game is known for its open-ended gameplay, allowing players to approach objectives in various ways, and its cutting-edge graphics that set a new benchmark for the industry.",
    image:
      "https://static.toiimg.com/thumb/msid-75198022,imgsize-1756086,width-400,resizemode-4/75198022.jpg",
    price: "2999",
    category: "game",
  },
  {
    id: 5,
    title: "Metro Exodus",
    description:
      "Metro Exodus is a first-person shooter with survival horror and stealth elements, developed by 4A Games. Set in a post-apocalyptic world devastated by nuclear war, players follow the story of Artyom as he leads a group of survivors on a journey across Russia to find a new home. The game features a mix of linear and open-world levels, each with its own distinct environment and challenges. Players must scavenge for resources, craft equipment, and manage their supplies while facing hostile humans and mutated creatures. The game emphasizes narrative-driven gameplay, with a strong focus on atmosphere and storytelling.",
    image:
      "https://cdn.mobygames.com/covers/7230960-metro-exodus-windows-apps-front-cover.jpg",
    price: "3999",
    category: "game",
  },
  {
    id: 6,
    title: "Sekiro: Shadows Die Twice",
    description:
      "Sekiro: Shadows Die Twice is an action-adventure game developed by FromSoftware and published by Activision. Set in a reimagined late 1500s Sengoku period Japan, players control a shinobi named Wolf who seeks revenge on a samurai clan that kidnapped his lord. The game is known for its challenging combat system, which emphasizes precise timing, parrying, and counter-attacks. Stealth mechanics allow players to approach enemies quietly and eliminate them without detection. The game features a deep narrative with multiple endings, influenced by the player's choices and actions throughout the journey.",
    image:
      "https://m.media-amazon.com/images/I/61M0jXMBbiL._SX522_.jpg",
    price: "5999",
    category: "game",
  },
  {
    id: 7,
    title: "Red Dead Redemption 2",
    description:
      "Red Dead Redemption 2 is an open-world action-adventure game developed and published by Rockstar Games. Set in the late 1800s American frontier, players control Arthur Morgan, a member of the Van der Linde gang, as they navigate a world filled with moral choices, dynamic events, and a richly detailed environment. The game features a vast open world with diverse landscapes, from dense forests to arid deserts, and bustling towns. Players can engage in a variety of activities, including hunting, fishing, and playing mini-games, while building relationships with other gang members. The story explores themes of loyalty, betrayal, and survival in a changing world.",
    image:
      "https://m.media-amazon.com/images/I/71xeX+uUF1L._AC_SX679_.jpg",
    price: "5999",
    category: "game",
  },
  {
    id: 8,
    title: "Brothers in Arms: Hell's Highway",
    description:
      "Brothers in Arms: Hell's Highway is a tactical first-person shooter developed by Gearbox Software and published by Ubisoft. Set during World War II, the game follows the story of Sergeant Matt Baker and his squad as they fight in Operation Market Garden, one of the largest airborne operations of the war. The game combines squad-based tactics with intense first-person combat, allowing players to command their squad to perform various maneuvers such as flanking and suppressing fire. The narrative is heavily focused on the bonds between soldiers and the emotional impact of war.",
    image: "https://i.ytimg.com/vi/uIajxxa8CmY/maxresdefault.jpg",
    price: "1999",
    category: "game",
  },
  {
    id: 9,
    title: "Grand Theft Auto V (GTA 5)",
    description:
      "Grand Theft Auto V (GTA 5) is an open-world action-adventure game developed and published by Rockstar Games. Set in the fictional state of San Andreas, which is based on Southern California, players switch between three protagonists: Michael, a retired bank robber; Franklin, a street gangster; and Trevor, a violent psychopath. The game features a massive open world with detailed environments, ranging from urban areas to rural countryside. Players can engage in a variety of activities, including completing missions, participating in heists, and exploring the world. The game also includes a robust online multiplayer mode, Grand Theft Auto Online, where players can create their own character and participate in various cooperative and competitive activities.",
    image:
      "https://s3.gaming-cdn.com/images/products/4211/orig/grand-theft-auto-v-premium-online-edition-cover.jpg",
    price: "2999",
    category: "game",
  },
  {
    id: 10,
    title: "DOOM Eternal",
    description:
      "DOOM Eternal is a fast-paced first-person shooter developed by id Software and published by Bethesda Softworks. Players take on the role of the Doom Slayer, a powerful warrior who battles demonic forces across various locations on Earth and beyond. The game emphasizes aggressive combat, requiring players to constantly move, dodge, and use a variety of weapons and abilities to dispatch enemies. The game features a deep lore, with a storyline that expands on the Doom universe, and introduces new gameplay mechanics such as platforming and resource management. DOOM Eternal is praised for its intense action, fluid movement, and challenging difficulty.",
    image:
      "https://th.bing.com/th/id/OIP.i1zM8F3GK4aI0lfMUOxumwHaEK?rs=1&pid=ImgDetMain",
    price: "4999",
    category: "game",
  },
  {
    id: 11,
    title: "Logitech G Pro X Gaming Headset",
    description:
      "The Logitech G Pro X Gaming Headset features advanced Blue VO!CE microphone technology for clear and professional voice communication. It offers 7.1 surround sound for immersive audio experience, and memory foam ear pads for extended comfort during long gaming sessions. The headset is designed for esports professionals and serious gamers.",
    image:
      "https://m.media-amazon.com/images/I/31mnI1bHllL._SY300_SX300_QL70_FMwebp_.jpg",
    price: "12999",
    category: "acc",
  },
  {
    id: 12,
    title: "Razer DeathAdder Elite Gaming Mouse",
    description:
      "The Razer DeathAdder Elite Gaming Mouse is equipped with a 16,000 DPI optical sensor for precise tracking. It features Razer's mechanical mouse switches for fast response times and durability. The ergonomic design ensures comfortable use during long gaming sessions, and the customizable Chroma RGB lighting adds a personalized touch.",
    image:
      "https://th.bing.com/th/id/OIP.jbCWX80eb8x9Us1WTzblvQHaE8?rs=1&pid=ImgDetMain",
    price: "6999",
    category: "acc",
  },
  {
    id: 13,
    title: "Corsair K95 RGB Platinum Mechanical Gaming Keyboard",
    description:
      "The Corsair K95 RGB Platinum is a high-performance mechanical gaming keyboard with Cherry MX Speed switches for fast and responsive keystrokes. It features dynamic RGB backlighting, a durable aluminum frame, and six programmable macro keys. The keyboard also includes a detachable wrist rest for added comfort.",
    image:
      "https://th.bing.com/th/id/OIP.eGX_Pe8SKntET_abGyfV3QHaDv?rs=1&pid=ImgDetMain",
    price: "19999",
    category: "acc",
  },
  {
    id: 14,
    title: "SteelSeries QcK Gaming Surface",
    description:
      "The SteelSeries QcK Gaming Surface is a high-quality mouse pad designed for precise mouse tracking. It features a micro-woven cloth surface optimized for both optical and laser sensors. The non-slip rubber base ensures stability during intense gaming sessions, and its durable design makes it a favorite among esports professionals.",
    image:
      "https://i.pinimg.com/originals/80/90/37/809037e8a47eac8afed6478b53537a23.jpg",
    price: "1499",
    category: "acc",
  },
  {
    id: 15,
    title: "Xbox Elite Wireless Controller Series 2",
    description:
      "The Xbox Elite Wireless Controller Series 2 offers extensive customization options, including adjustable-tension thumbsticks, interchangeable components, and programmable buttons. It features up to 40 hours of rechargeable battery life and a refined design for enhanced comfort and performance. This controller is designed for serious gamers who demand the best.",
    image:
      "https://compass-ssl.xboxlive.com/assets/ed/6f/ed6fc61a-7788-49ee-b715-ac14687d98e3.jpg?n=SXC-Articles-RGB-16x9-794x447-02.jpg",
    price: "17999",
    category: "acc",
  },
  {
    id: 16,
    title: "Elgato Stream Deck",
    description:
      "The Elgato Stream Deck is a powerful tool for content creators, offering 15 customizable LCD keys that can be assigned to various functions such as launching applications, controlling streams, and executing complex commands. The Stream Deck integrates seamlessly with popular streaming platforms and software, making it an essential accessory for streamers and video producers.",
    image:
      "https://th.bing.com/th/id/OIP.pqpcufDXMpcJuhOGGILaWgAAAA?rs=1&pid=ImgDetMain",
    price: "14999",
    category: "acc",
  },
  {
    id: 17,
    title: "PlayStation VR",
    description:
      "The PlayStation VR headset delivers an immersive virtual reality experience for PlayStation 4 and PlayStation 5 users. It features a 5.7-inch OLED display with a 120 Hz refresh rate for smooth visuals and a 3D audio system for enhanced sound immersion. The headset is comfortable to wear and easy to set up, providing access to a wide range of VR games and experiences.",
    image: "https://techdaring.com/wp-content/uploads/2016/01/PS-VR.jpg",
    price: "29999",
    category: "acc",
  },
  {
    id: 18,
    title: "HyperX Cloud II Gaming Headset",
    description:
      "The HyperX Cloud II Gaming Headset offers virtual 7.1 surround sound for immersive gaming audio. It features a noise-canceling microphone for clear communication, memory foam ear pads for comfort, and durable aluminum frame construction. The headset is compatible with multiple platforms, including PC, consoles, and mobile devices.",
    image:
      "https://th.bing.com/th/id/R.4c93081be9916c7336fff91be52aab9e?rik=Ct3Hkt9%2fd2vA6Q&riu=http%3a%2f%2fassets1.ignimgs.com%2f2017%2f01%2f20%2fkingston-headset-1280-1484940785968_1280w.jpg&ehk=YLTLGLMM8XxWtNw9973o9G8Ogwrydv2GH3DAiJeE9F0%3d&risl=&pid=ImgRaw&r=0",
    price: "9999",
    category: "acc",
  },
  {
    id: 19,
    title: "ASUS ROG Swift PG279Q Gaming Monitor",
    description:
      "The ASUS ROG Swift PG279Q is a 27-inch gaming monitor with a WQHD resolution of 2560x1440, delivering stunning visuals and sharp details. It features a 165 Hz refresh rate and G-SYNC technology for smooth, tear-free gameplay. The monitor also includes customizable RGB lighting and ergonomic adjustments for a comfortable viewing experience.",
    image: "https://www.cclonline.com/images/avante/PG279Q.jpg",
    price: "79999",
    category: "acc",
  },
  {
    id: 20,
    title: "NVIDIA Shield TV Pro",
    description:
      "The NVIDIA Shield TV Pro is a powerful streaming media player that also doubles as a gaming console. It supports 4K HDR streaming, AI upscaling, and has access to a wide range of apps and games through the Google Play Store. The device is equipped with the NVIDIA Tegra X1+ processor, providing fast and smooth performance for gaming and entertainment.",
    image:
      "https://m.media-amazon.com/images/I/31rhQ3TEZSL._SY300_SX300_QL70_FMwebp_.jpg",
    price: "19999",
    category: "acc",
  },
];

const GAME_ACC_DATA = [
  {
    id: 11,
    title: "Logitech G Pro X Gaming Headset",
    description:
      "The Logitech G Pro X Gaming Headset features advanced Blue VO!CE microphone technology for clear and professional voice communication. It offers 7.1 surround sound for immersive audio experience, and memory foam ear pads for extended comfort during long gaming sessions. The headset is designed for esports professionals and serious gamers.",
    image: "https://example.com/logitech_g_pro_x.jpg",
    price: "129.99",
    category: "acc",
  },
  {
    id: 12,
    title: "Razer DeathAdder Elite Gaming Mouse",
    description:
      "The Razer DeathAdder Elite Gaming Mouse is equipped with a 16,000 DPI optical sensor for precise tracking. It features Razer's mechanical mouse switches for fast response times and durability. The ergonomic design ensures comfortable use during long gaming sessions, and the customizable Chroma RGB lighting adds a personalized touch.",
    image: "https://example.com/razer_deathadder_elite.jpg",
    price: "69.99",
    category: "acc",
  },
  {
    id: 13,
    title: "Corsair K95 RGB Platinum Mechanical Gaming Keyboard",
    description:
      "The Corsair K95 RGB Platinum is a high-performance mechanical gaming keyboard with Cherry MX Speed switches for fast and responsive keystrokes. It features dynamic RGB backlighting, a durable aluminum frame, and six programmable macro keys. The keyboard also includes a detachable wrist rest for added comfort.",
    image: "https://example.com/corsair_k95_rgb_platinum.jpg",
    price: "199.99",
    category: "acc",
  },
  {
    id: 14,
    title: "SteelSeries QcK Gaming Surface",
    description:
      "The SteelSeries QcK Gaming Surface is a high-quality mouse pad designed for precise mouse tracking. It features a micro-woven cloth surface optimized for both optical and laser sensors. The non-slip rubber base ensures stability during intense gaming sessions, and its durable design makes it a favorite among esports professionals.",
    image: "https://example.com/steelseries_qck.jpg",
    price: "14.99",
    category: "acc",
  },
  {
    id: 15,
    title: "Xbox Elite Wireless Controller Series 2",
    description:
      "The Xbox Elite Wireless Controller Series 2 offers extensive customization options, including adjustable-tension thumbsticks, interchangeable components, and programmable buttons. It features up to 40 hours of rechargeable battery life and a refined design for enhanced comfort and performance. This controller is designed for serious gamers who demand the best.",
    image: "https://example.com/xbox_elite_series_2.jpg",
    price: "179.99",
    category: "acc",
  },
  {
    id: 16,
    title: "Elgato Stream Deck",
    description:
      "The Elgato Stream Deck is a powerful tool for content creators, offering 15 customizable LCD keys that can be assigned to various functions such as launching applications, controlling streams, and executing complex commands. The Stream Deck integrates seamlessly with popular streaming platforms and software, making it an essential accessory for streamers and video producers.",
    image: "https://example.com/elgato_stream_deck.jpg",
    price: "149.99",
    category: "acc",
  },
  {
    id: 17,
    title: "PlayStation VR",
    description:
      "The PlayStation VR headset delivers an immersive virtual reality experience for PlayStation 4 and PlayStation 5 users. It features a 5.7-inch OLED display with a 120 Hz refresh rate for smooth visuals and a 3D audio system for enhanced sound immersion. The headset is comfortable to wear and easy to set up, providing access to a wide range of VR games and experiences.",
    image: "https://example.com/playstation_vr.jpg",
    price: "299.99",
    category: "acc",
  },
  {
    id: 18,
    title: "HyperX Cloud II Gaming Headset",
    description:
      "The HyperX Cloud II Gaming Headset offers virtual 7.1 surround sound for immersive gaming audio. It features a noise-canceling microphone for clear communication, memory foam ear pads for comfort, and durable aluminum frame construction. The headset is compatible with multiple platforms, including PC, consoles, and mobile devices.",
    image: "https://example.com/hyperx_cloud_ii.jpg",
    price: "99.99",
    category: "acc",
  },
  {
    id: 19,
    title: "ASUS ROG Swift PG279Q Gaming Monitor",
    description:
      "The ASUS ROG Swift PG279Q is a 27-inch gaming monitor with a WQHD resolution of 2560x1440, delivering stunning visuals and sharp details. It features a 165 Hz refresh rate and G-SYNC technology for smooth, tear-free gameplay. The monitor also includes customizable RGB lighting and ergonomic adjustments for a comfortable viewing experience.",
    image: "https://example.com/asus_rog_swift_pg279q.jpg",
    price: "799.99",
    category: "acc",
  },
  {
    id: 20,
    title: "NVIDIA Shield TV Pro",
    description:
      "The NVIDIA Shield TV Pro is a powerful streaming media player that also doubles as a gaming console. It supports 4K HDR streaming, AI upscaling, and has access to a wide range of apps and games through the Google Play Store. The device is equipped with the NVIDIA Tegra X1+ processor, providing fast and smooth performance for gaming and entertainment.",
    image: "https://example.com/nvidia_shield_tv_pro.jpg",
    price: "199.99",
    category: "acc",
  },
];

const apiCaller = () => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res(GAMES_DATA);
    }, 1200);
  });
};

async function fetchFromApi(path) {
  // const res = await fetch(`${baseUrl}/${path}`);
  // const data = await res.json();
  // return data;
  return await apiCaller();
}

export default fetchFromApi;
